﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
	public static GameManager instance; // gamemanager instance variable
	public GameObject player; // stores player character
	public GameObject mainmenu; // stores game scene
	public GameObject level1; // stores game scene
	public GameObject level1dash2; // stores game scene
	public GameObject level2; // stores game scene
	public GameObject level2dash3; // stores game scene
	public GameObject level3; // stores game scene
	public GameObject winscreen; // stores game scene
	public GameObject losescreen; // stores game scene
	public GameObject level1end; // stores level end triggerbox
	public GameObject level2end; // stores level end triggerbox
	public GameObject level3end; // stores level end triggerbox
	public List<GameObject> enemies; // stores all enemies
	
	public float lives; // float to store current lives
	public float maxLives; // float to store max lives
	
	// Use this for initialization
	void Awake () 
		{
			if (instance == null) // if no instance
			{
				instance = this; // this is it
				DontDestroyOnLoad(gameObject); // dont destroy it
			}
			else// otherwise
			{
				Destroy(gameObject); // destroy this
			}
			instance = this; // this is the instance
		}
	
	void Update()
	{
		if (lives <= 0)
		{
			winscreen.SetActive(false); // deactivates scene
			mainmenu.SetActive(false); // deactivates scene
			level1.SetActive(false); // deactivates scene
			level1dash2.SetActive(false); // deactivates scene
			level2.SetActive(false); // deactivates scene
			level2dash3.SetActive(false); // deactivates scene
			level3.SetActive(false); // deactivates scene
			losescreen.SetActive(true); //activates scene
			player.transform.position = new Vector3 (0, 0, 0); // places player at origin
			lives = maxLives; // resets lives to max
			

		}
	}
	public void Levelone () 
	{
		level1.SetActive(true); //activates scene
		mainmenu.SetActive(false); // deactivates scene
		player.transform.position = new Vector3(0, 0, 0); // places player at origin
	}
	
	public void Leveltwo ()
	{
		level1dash2.SetActive(false); // deactivates scene
		level2.SetActive(true); //activates scene
		player.transform.position = player.GetComponent<PlayerScript>().startPos; // changes player location to last checkpoint
	}

	public void Levelthree ()
	{
		level2dash3.SetActive(false); // deactivates scene
		level3.SetActive(true); //activates scene
		player.transform.position = player.GetComponent<PlayerScript>().startPos; // changes player location to last checkpoint
	}	
	
	public void ResetGame()
	{
		winscreen.SetActive(false); // deactivates scene
		losescreen.SetActive(false); // deactivates scene
		lives = maxLives;
		foreach(GameObject i in enemies)
		{
			i.SetActive(true); // re-activates  any deacivated enemies
		}
		mainmenu.SetActive(true); //activates scene
		player.transform.position = new Vector3 (0, 0, 0); // places player at origin
	}
	public void Quit()
	{
		Application.Quit(); // quits application
	}
}
