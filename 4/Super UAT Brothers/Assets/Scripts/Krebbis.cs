﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Krebbis : MonoBehaviour 
{
	public Transform WP1; // transform for left waypoint
	public Transform WP2; // transform for right waypoint
	public bool goingleft; // bool to determine direction
	public Transform tf; // object transform
	
	// Use this for initialization
	void Start () 
	{
		tf = GetComponent<Transform>(); // sets object transform
		GameManager.instance.enemies.Add(this.gameObject); // adds senemy to list in gamemanager
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(tf.position.x <= WP1.transform.position.x)
		{
			goingleft = false; // sets bool to false
			GetComponent<SpriteRenderer>().flipX = true; // flips spriterenderer
		}
		
		if(tf.position.x >= WP2.transform.position.x)
		{
			goingleft = true; // sets bool to false
			GetComponent<SpriteRenderer>().flipX = false; // flips spriterendered
		}
		
		
		if(goingleft)
		{
			tf.Translate(Vector3.left * Time.deltaTime); // moves object left 
		}
		if(!goingleft)
		{
			tf.Translate(Vector3.right * Time.deltaTime); // moves object right
		}
	}
	
	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.name.Contains("Player"))
		{
			GameManager.instance.player.GetComponent<PlayerScript>().Die(); // calls die function of player object
		}
	}
}
