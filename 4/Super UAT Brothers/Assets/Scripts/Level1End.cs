﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level1End : MonoBehaviour 
{
	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.name.Contains("Player"))
		{
			GameManager.instance.level1.SetActive(false); // deactivates scene
			GameManager.instance.player.transform.position = new Vector3 (0, 0, 0); // places player at origin
			GameManager.instance.level1dash2.SetActive(true); //activates scene
		}
	}
}
