﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointManager : MonoBehaviour 
{
	public Transform tf; // stores this objects transform
	
	
	void Start()
	{
		tf = GetComponent<Transform>(); // sets this objects transform
	}
	
	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.name.Contains("Player"))
		{
			coll.gameObject.GetComponent<PlayerScript>().startPos = tf.position; // reseets player starpos to this object's location
		}
	}

}
