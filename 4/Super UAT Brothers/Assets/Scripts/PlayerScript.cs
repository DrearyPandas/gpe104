﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour 
{

	public Transform tf; // stores transform component
	public Rigidbody2D rb; // stores rigidbody component
	public Vector3 startPos; // stores player spawn location
	public float moveSpeed; // float to store the player move speed
	public float jumpStr; // float to store the jump strength
	public float jumpLength; // float to store the max jump time
	public float jumpCurrent; // float to store the current jump time
    public bool grounded; // bool to store whether the player is on the ground
    public LayerMask Ground; // stores the layer to check for overlap
    public bool releasedJump; // bool to store if the player has released the jump key yet
    public Transform groundCheck; // stores the position of the groundchecker gameobject
    public float groundCheckRadius; // float to store the radius of the groundchecker physics overlap circle
	public bool jumping; // bool to store animation state
	public bool movingLeft; // bool to store animation state
	public bool movingRight; // bool to store animation state
	public bool idle; // bool to store animation state
	public SpriteRenderer sr; // stores spriterender component
	public Animator anim; // stores animator component
	public AudioClip jump; // stores jump noise
	public AudioSource audio; // stores audio source component
	public AudioClip die; // sotre death sound audioclip
	public float jumpNumber; // stores the remaining number of jumps
	public float jumpMax; // stores the maximum numer of jumps
	
	
	// Use this for initialization
	void Start () 
	{
		GameManager.instance.player = this.gameObject; // sets gamemanager variable to this object
		rb = GetComponent<Rigidbody2D>(); // gets and sets rigidbody component
		tf = GetComponent<Transform>(); // gets and sets transform component
		startPos = tf.position; // sets startposition to current position
		jumpCurrent = jumpLength; // sets current jump time to max jump time
		jumpNumber = jumpMax; // sets the current number of jumps to the max
		sr = GetComponent<SpriteRenderer>(); // gets and sets Spriterenderer component
		anim = GetComponent<Animator>(); // gets and sets animator component
		audio = GetComponent<AudioSource>(); // gets and sets audiosource component
	}
	
	// Update is called once per frame
	void Update () 
	{
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, Ground); // checks if the groundchecker gamepbject is overlapped with any object in the ground layer and sets boolean
		
        if(grounded)
        {
			jumping = false; // sets jumping to false
			jumpNumber = jumpMax; // resets current number of jumps to max
            jumpCurrent = jumpLength; // sets current jump timer to max jump time
        }
		if(!grounded)
		{
			idle = false; // sets bool to false
			jumping = true; // sets bool to true
		}
		
		if (Input.GetKey(KeyCode.A))
		{
			tf.Translate(Vector3.left * (Time.deltaTime * moveSpeed)); // moves the player left at time times movespeed
			idle = false; // sets bool to false
			movingLeft = true; // sets bool to true
			movingRight = false; // sets bool to false
			sr.flipX = true; // flips the sprite to face left
		}
		
		if (Input.GetKey(KeyCode.D))
		{
			tf.Translate(Vector3.right * (Time.deltaTime * moveSpeed)); // moves the player right at time times movespeed
			idle = false; // sets bool to false
			movingLeft = false; // sets bool to false
			movingRight = true; // sets bool to true
			sr.flipX = false; // flips the sprite to face right
		}
			
		if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space)) 
		{
			if (jumpNumber > 0 && !grounded)
			{
				rb.velocity = new Vector3 (rb.velocity.x, jumpStr); // sets player velocity to jumpstrength in the positive x direction
				jumpNumber -= 1; // decrements current jump number
			}			
            if(grounded)
            {
                rb.velocity = new Vector3 (rb.velocity.x, jumpStr); // sets player velocity to jumpstrength in the positive x direction
                releasedJump = false; // sets bool to false
				idle = false; // sets bool to false
				jumping = true; // sets bool to true
				grounded = false; // sets bool to false
				audio.PlayOneShot(jump); // plays jump noise
				jumpNumber -= 1; // decrements current jump number
            }
		}
		
        if(Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.Space))
        {
            jumpCurrent = 0; // sets current jump timer to zero
            releasedJump = true; // sets bool to true
        }
		
        if((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.Space)) && !releasedJump)
        {
            if(jumpCurrent > 0)
            {
                rb.velocity = new Vector3 (rb.velocity.x, jumpStr); // sets player velocity to jumpstrength in the positive x direction
                jumpCurrent -= Time.deltaTime; // decrements jumpcurrent timer
            }
        }
		
		if (!Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D) && !jumping)
		{
			movingLeft = false; // sets bool to false
			movingRight = false; // sets bool to false
			idle = true; // sets bool to true
		}
		
		if(movingLeft)
		{
			if(!jumping)
			{
				anim.Play("Run"); // plays the run animation
			}
		}
		if(movingRight)
		{
			if(!jumping)
			{
				anim.Play("Run"); // plays the run animation
			}
		}
		if(jumping)
		{
			anim.Play("Jump"); // plays the jump animation
		}
		if(idle)
		{
			anim.Play("Idle"); // plays the idle animation
		}
		
	}
	
	public void Die()
	{
		tf.position = startPos; // resets player position
		audio.PlayOneShot(die); // plays audioclip
		GameManager.instance.lives -= 1; // decrements lives
	}
}
