﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillVolume : MonoBehaviour 
{

	void OnTriggerEnter2D(Collider2D coll)
	{
		coll.gameObject.GetComponent<PlayerScript>().Die(); // calls the Die function of the collided object's playerscript (player)
	}
}



