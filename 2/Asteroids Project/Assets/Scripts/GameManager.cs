﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
	
	public static GameManager instance; // gamemanager initialization
	public Player player; // player script reference
	public GameObject ship; //player controller reference
	public List<GameObject> enemies; // list to store enemies
	public float score; // stores score
	public int lives; // stores life total
	public List<GameObject> spawnPoints; // list to store all enemy spawnpoints
	public GameObject Asteroid; // stores Asteroid object
	public GameObject EnemyShip; // Stores enemy ship object
	public int num; // stores random spawnpoint number
	public int spawnChoice; // stores random number for which enemy to spawn
	public GameObject playerSpawn; // stores object for player's spawnpoint

	
	public bool isPlayerAlive = true;// sets and stores player alive boolean
	
	// This is the same as Start, except it runs before Start
	void Awake () 
	{
		if (instance == null) // if no instance
		{
			instance = this; // this is it
			DontDestroyOnLoad(gameObject); // dont destroy it
		}
		else// otherwise
		{
			Destroy(gameObject); // destroy this
		}
		instance = this; // this is the instance
		ship = Instantiate<GameObject>(ship, GameManager.instance.playerSpawn.transform.position, Quaternion.identity); // creates ship at ship spawnpoint
		ship.SetActive(true); // sets ship to active
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		if(enemies.Count < 3) // if less than 3 enemies exist
		{
			num = Random.Range(0,spawnPoints.Count);// get a number between 0 and how many spawnpoints there are
			GameObject point = spawnPoints[num]; // store the object for what spawnpoint in the list was randomly generated
			spawnChoice = Random.Range(0, 2); // generates a random number between 0 and 2 and stores
			
			if (spawnChoice == 1)// if 1 was chosen for spawnchoice
			{
				GameObject asteroid = Instantiate<GameObject>(Asteroid, point.transform.position, Quaternion.identity);// spawn an asteroid prefab at the chosen point and labels it asteroid
			}
			else
			{
				GameObject enemyShip = Instantiate<GameObject>(EnemyShip, point.transform.position, Quaternion.identity); // spawns an enemy ship prefab at chosen point and labels it enemyShip
			}
		}
		isPlayerAlive = true; // sets playeralive boolean to true
		if (GameManager.instance.lives <= 0)// if lives is less than or equal to zero
		{
			Application.Quit(); // quits application
		}
	}
	
	public void Respawn()
	{
		isPlayerAlive = false; // sets playeralive boolean to false
		GameManager.instance.ship.SetActive(false); // deactivates the ship prefab
		ship = Instantiate<GameObject>(ship, GameManager.instance.playerSpawn.transform.position, Quaternion.identity);// creates a new ship prefab at the playerspawnpoint and labels it ship
		ship.SetActive(true); // activates new prefab
		
		if (isPlayerAlive == false) // if playeralive is false
		{
			isPlayerAlive = true; // set it to true
		}
	}

}
