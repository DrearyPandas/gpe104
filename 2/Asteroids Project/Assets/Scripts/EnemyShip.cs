﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShip : MonoBehaviour 
{

	public float health; // stores object's health //unused
	public float movespeed; // store object's movespeed
	public float damage; // stores object's damage // unused
	private Transform tf; // stores object transform
	private GameObject enemyShip; // stores object gameobject
	public float rotateSpeed; // stores rotation speed


	// Use this for initialization
	void Start () 
	{
		GameManager.instance.enemies.Add(this.gameObject); // adds this object to the enemies list
		tf = this.transform; // stores this object's transform
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		tf.position = tf.position + tf.up * movespeed; // additive movement in the forward direction
		
		if (GameManager.instance.isPlayerAlive == true) // if the playeralive boolean is true
		{
		Vector3 localPosition = GameManager.instance.player.transform.position - tf.position; // take difference of player location and self location
		localPosition = localPosition.normalized; // normalize vector
		float angle = Mathf.Atan2(localPosition.y, localPosition.x) * Mathf.Rad2Deg; // vector to degrees
		transform.rotation = Quaternion.Lerp((this.gameObject.transform.rotation), Quaternion.Euler(0f, 0f, angle - 90), (Time.deltaTime * rotateSpeed)); // lerping quaternions in order to scale rotation speed
		}
	}
	
	void OnDestroy() // when destroyed
	{
		GameManager.instance.enemies.Remove(this.gameObject); // remove object from list
	}
	
	void OnCollisionEnter2D(Collision2D coll) // on collision
	{
		if (coll.gameObject.name.Contains("Player")) // if the collided object's name contains Player
		{
			GameManager.instance.player.Destroyed(); // run the Destroyed function from the player script
		}
	}
}
