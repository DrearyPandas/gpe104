﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScript : MonoBehaviour 
{
	public Text t; // creates public text variable
	
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		t.text = ("Points: " + GameManager.instance.score); // sets text to Points: + the score
	}
}
