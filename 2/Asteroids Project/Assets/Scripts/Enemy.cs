﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour 
{

	public float health; // stores health value // unused
	public float movespeed; // store movement speed
	public float damage; // stores damage // unused
	private Transform tf; // stores object transform
	private GameObject Asteroid; // store object gameobject


	// Use this for initialization
	void Start () 
	{
		GameManager.instance.enemies.Add(this.gameObject); // add this object to enemies list
		tf = this.transform; // sets the transform to this objects transfrom

		if (GameManager.instance.isPlayerAlive == true) // if playeralive bool is true
		{
			Vector3 localPosition = GameManager.instance.player.transform.position - tf.position; // take difference of player location and self location
			localPosition = localPosition.normalized; // normalize
			float angle = Mathf.Atan2(localPosition.y, localPosition.x) * Mathf.Rad2Deg; // vector3 to degrees
			transform.rotation = Quaternion.Euler(0f, 0f, angle); // sets rotation to face player
		}
	}
	
	// Update is called once per frame
	void Update () 
	{

		tf.position = tf.position + tf.right * movespeed; // additive movement scaled to movespeed
		

	}
	
	void OnDestroy() // when destroyed
	{
		GameManager.instance.enemies.Remove(this.gameObject); // remove object from enemies list
	}
	
	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.name.Contains("Player")) // if collided object name contains Player
		{
			GameManager.instance.player.Destroyed(); // run the Destroyed function in player script
			GameManager.instance.isPlayerAlive = false; // set the playeralive bool to false
		}
	}
}
