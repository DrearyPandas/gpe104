﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootScript : MonoBehaviour 
{

	public GameObject Bullet; // stores bullet gameobject
	private Transform BarrelTF; // stores barrel transform
	public float bulletSpeed; // stores barrel movespeed
	

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Space))
		{
			GameObject bullet = (GameObject)Instantiate(Bullet, GameManager.instance.player.Bulletspawn.transform.position, Quaternion.identity); // spawns bullet at bulletspawner and labels it bullet with forward rotation
			bullet.GetComponent<Rigidbody2D>().AddForce(GameManager.instance.player.Bulletspawn.transform.up * bulletSpeed); //adds force to bullet in forward direction multiplied by bullet speed
		}
	}
}
