﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour 
{
	public float bulletLife; // stores max bullet time
	public float timer; // stores current bullet time

	// Use this for initialization
	void Start () 
	{
		timer = 0; // resets timer to zero
	}
	
	// Update is called once per frame
	void Update () 
	{
		timer += 1; // increments timer
		if (timer >= bulletLife)
		{
			Destroy(this.gameObject);// destroys object
		}
	}
	
	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.name == "Asteroid(Clone)")
		{
			GameObject.Destroy(coll.gameObject);// destroys collided object
			GameManager.instance.score = GameManager.instance.score + 1; // increments score
			Object.Destroy(this.gameObject); // destroys self
		}
		
		
		if (coll.gameObject.name == "EnemyShip(Clone)")
		{
			GameObject.Destroy(coll.gameObject); // destroys collided object
			GameManager.instance.score = GameManager.instance.score + 2; // increments score
			Object.Destroy(this.gameObject); // destroys self
		}
	}
}
