﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Player : MonoBehaviour 
{
	public GameObject player;
	public float moveSpeed;//visible variable in editor to store move speed modifier
	public float health;//visible variable in editor to store health count
	public float turnSpeed;//visible variable in editor to store turn speed modifier
	public GameObject Bulletspawn; // stores bulletspawn gameobject
	
	// Use this for initialization
	void Start () 
	{
		GameManager.instance.player = this; // initializes this object as a player for the gameobject
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		if (Input.GetKey(KeyCode.A)) // detects keyboard input for A
		{

			GameManager.instance.player.transform.rotation = GameManager.instance.player.transform.rotation * Quaternion.Euler(0, 0, turnSpeed); // Singleton pattern for rotation in the  direction of turnspeed
		}
		
		if (Input.GetKey(KeyCode.D)) // detects keyboard input for D
		{
			GameManager.instance.player.transform.rotation = GameManager.instance.player.transform.rotation * Quaternion.Euler(0, 0, -turnSpeed); // Singleton pattern for rotation in the opposite direction as turnspeed
		}
		
		if (Input.GetKey(KeyCode.W)) // detects keyboard input for W
		{
			//playerT.position = playerT.position + transform.up * moveSpeed;
			GameManager.instance.player.transform.position = GameManager.instance.player.transform.position + transform.up * moveSpeed;// Singleton pattern for additive movement in direction related to rotation
		}
		
		if (Input.GetKey(KeyCode.S)) // detects keyboard input for S
		{
			//playerT.position = playerT.position + transform.up * -moveSpeed;
			GameManager.instance.player.transform.position = GameManager.instance.player.transform.position + transform.up * -moveSpeed; // Singleton pattern for additive movement in negative direction related to rotation
		}
	}
	
	public void Destroyed () 
	{
		
		GameManager.instance.lives -= 1; // decrement lives
		foreach (GameObject GameObject in GameManager.instance.enemies) // does process for each object in list
		{
			Destroy(GameObject); // destroys gameobject
			
		}
		GameManager.instance.Respawn(); // calls the respawn function from the GameManager script
	}
}
