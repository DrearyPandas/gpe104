﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillVolume : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
	
	void OnCollisionEnter2D (Collision2D coll)
	{
		if (coll.gameObject.name.Contains("Player")) // if the collided object's name contains Player
		{
			GameManager.instance.player.Destroyed();// run the Destroyed function from the player script
		}
		else // otherwise
		{
			Destroy(coll.gameObject); // destroy the object
		}
	}
}
