﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : Controller 
{
	public bool canSeePlayer = false;
	public bool canHearPlayer = false;
	// Use this for initialization
	public override void Start () 
	{
		GameManager.instance.AI = this;
		base.Start();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!canSeePlayer && !canHearPlayer)
		{
			pawn.Idle();
		}
		if(canSeePlayer)
		{
			pawn.SeePlayer();
		}
		if(canHearPlayer)
		{
			pawn.HearPlayer();
		}
	}
}
