﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Followenemy : MonoBehaviour 
{

	private Transform tf; // stores transform for self
	private Vector3 targetTF; // stores transform for target location
	// Use this for initialization
	void Start () 
	{
		tf = GetComponent<Transform>(); // stores transform data for object in variable
	}
	
	// Update is called once per frame
	void Update () 
	{
		targetTF = new Vector3 ((GameManager.instance.Enemy.transform.position.x + .5f), GameManager.instance.Enemy.transform.position.y, -5); //stores desired location of object
		tf.position = targetTF; // sets transform to desired location
	}
}
