﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour 
{
	public Transform Tf; // stores transform of object
	public Vector3 home; // stores home position of object
	public float moveSpeed = 0.1f; // stores base class movespeed
	public float rotateSpeed = 5f; // stores base class rotation speed

	
	// Use this for initialization
	public virtual void Start () 
	{
		Tf = GetComponent<Transform>(); // gets and sets object transform
		home = Tf.position; // sets object's home position
	}
	
	// Update is called once per frame
	public virtual void Update () 
	{
		
	}
	public virtual void Forward () 
	{
		
	}
	public virtual void Left () 
	{
		
	}
	public virtual void Back () 
	{
		
	}
	public virtual void Right () 
	{
		
	}
	public virtual void Die () 
	{
		
	}
	public virtual void SeePlayer()
	{
		
	}
	public virtual void HearPlayer()
	{
		
	}
	public virtual void Idle()
	{
		
	}
	public virtual void Sprint()
	{
		
	}
	public virtual void StoppedSprint()
	{
		
	}
}
