﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Pawn 
{

	// Use this for initialization
	public override void Start () 
	{
		GameManager.instance.Player = this.gameObject; // sets this object to the player in the Gamemanager
		base.Start(); // calls base class start function
	}
	
	// Update is called once per frame
	public override void Update () 
	{
		base.Update(); // calls base class update function
	}
	public override void Forward ()
	{
		Tf.position = Tf.position + (Tf.up * moveSpeed); // moves the object forward * movespeed
	}
	public override void Right ()
	{
		Tf.rotation = Tf.rotation * Quaternion.Euler(0, 0, -rotateSpeed); // rotates the object * negative rotationspeed
	}
	public override void Back ()
	{
		Tf.position = Tf.position + (Tf.up * -moveSpeed); // moves the object forward *  negative movespeed
	}
	public override void Left ()
	{
		Tf.rotation = Tf.rotation * Quaternion.Euler(0, 0, rotateSpeed); // rotates the object * rotationspeed
	}
	public override void Die ()
	{
		Tf.position = home;// sets the position of the object to its home position
	}
	public override void Sprint()
	{
		moveSpeed = .1f; // sets movespeed 
		GameManager.instance.PC.noiseLevel = 5f; // sets noise level
	}
	public override void StoppedSprint()
	{
		moveSpeed = .05f; // sets movespeed
		GameManager.instance.PC.noiseLevel = 0f; // sets noiselevel
	}
}
