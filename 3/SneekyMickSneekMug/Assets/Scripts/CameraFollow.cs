﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour 
{
	private Transform tf; // stores transform for self
	private Vector3 targetTF; // stores transform for target location
	// Use this for initialization
	void Start () 
	{
		tf = GetComponent<Transform>(); // gets and stores transform for self in variable
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		targetTF = new Vector3 (GameManager.instance.Player.transform.position.x, GameManager.instance.Player.transform.position.y, -11); // stores desired location of object
		tf.position = targetTF; // sets transform to desired location
	}
}
