﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitGame : MonoBehaviour 
{
	public void DoExitGame() // function init
	{
		Application.Quit(); // exits game
	}
}
