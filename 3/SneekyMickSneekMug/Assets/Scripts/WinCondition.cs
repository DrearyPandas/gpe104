﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinCondition : MonoBehaviour 
{
	public GameObject Winscreen; // stores winscreen gameobject
	public GameObject Gameplay; // stores gameplay gameobject
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
	
	void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.name.Contains("Player"))
		{
			Gameplay.SetActive(false); // sets gameplay gameobject to false
			Winscreen.SetActive(true); // sets winscreen gameobject to true
		}
	}
}
