﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFov : MonoBehaviour 
{
	public bool playerInCollider = false; // var to store collider boolean
	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (playerInCollider)
		{
			Vector3 localPosition = GameManager.instance.Player.transform.position - GameManager.instance.Enemy.transform.position; // take difference of player location and self location
			localPosition = localPosition.normalized; // normalize vector
			RaycastHit2D hit = Physics2D.Raycast(GameManager.instance.Enemy.transform.position, localPosition, 999);// raycast from enemy origin in direction of player with very large range
			if(hit.collider.gameObject.name.Contains("Player"))
			{
				GameManager.instance.AI.canSeePlayer = true; // sets bool to true
			}
			else
			{
				GameManager.instance.AI.canSeePlayer = false; // sets bool to false
			}
		}
		else
		{
			GameManager.instance.AI.canSeePlayer = false; // sets bool to false
		}
	}
	void OnTriggerEnter2D(Collider2D coll)
	{
		GameManager.instance.AI.canSeePlayer = false; // sets bool to false
		if (coll.gameObject.name.Contains("Player"))
		{	
			playerInCollider = true; // sets bool to true
		}
	}
	void OnTriggerExit2D(Collider2D coll)
	{
		if (coll.gameObject.name.Contains("Player"))
		{
			playerInCollider = false; // sets bool to false
		}
	}
}
