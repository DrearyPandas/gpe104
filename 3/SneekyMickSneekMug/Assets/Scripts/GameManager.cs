﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
	public static GameManager instance; // var to store gamemanager instance
	public AIController AI; // var to store AI controller script
	public GameObject Player; // var to store player object
	public GameObject Enemy; // var to store enemy object
	public PlayerController PC; // var to store playercontroller script
	public GameObject LoseScreen; // var to store losescreen object set
	public GameObject GamePlay; // var to store gameplay object set

	// Use this for initialization
	void Awake () 
	{
		if (instance == null) // if no instance
		{
			instance = this; // this is it
			DontDestroyOnLoad(gameObject); // dont destroy it
		}
		else// otherwise
		{
			Destroy(gameObject); // destroy this
		}
		instance = this; // this is the instance
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
