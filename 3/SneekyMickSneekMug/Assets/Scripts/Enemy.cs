﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Pawn 
{

	public GameObject Alerted; // var to store gameobject
	// Use this for initialization
	public override void Start () 
	{
		GameManager.instance.Enemy = this.gameObject; // stores this object as gamemanager var 
		base.Start(); // calls base class start funct
	}
	
	// Update is called once per frame
	public override void Update () 
	{
		base.Update(); // calls base class update function
	}
	public override void SeePlayer()
	{
		
		Vector3 localPosition = GameManager.instance.Player.transform.position - Tf.position; // take difference of player location and self location
		localPosition = localPosition.normalized; // normalize vector
		float angle = Mathf.Atan2(localPosition.y, localPosition.x) * Mathf.Rad2Deg; // vector to degrees
		Tf.rotation = Quaternion.Lerp((this.gameObject.transform.rotation), Quaternion.Euler(0f, 0f, angle - 90), (Time.deltaTime * rotateSpeed * 8)); // lerping quaternions in order to scale rotation speed and rotate
		Tf.position = Tf.position + (Tf.up * moveSpeed);// moves object forward * movespeed
		Alerted.SetActive(true); // enables the alerted gameobject
	}
	public override void Idle()
	{
		Alerted.SetActive(false); // disables the alerted gameobject
		Tf.rotation = Tf.rotation * Quaternion.Euler(0, 0, (rotateSpeed / 2));
	}
	public override void HearPlayer()
	{
		Alerted.SetActive(true); // enables the alerted gameobject
		Vector3 localPosition = GameManager.instance.Player.transform.position - Tf.position; // take difference of player location and self location
		localPosition = localPosition.normalized; // normalize vector
		float angle = Mathf.Atan2(localPosition.y, localPosition.x) * Mathf.Rad2Deg; // vector to degrees
		Tf.rotation = Quaternion.Lerp((this.gameObject.transform.rotation), Quaternion.Euler(0f, 0f, angle - 90), (Time.deltaTime * rotateSpeed * 2)); // lerping quaternions in order to scale rotation speed and rotate
	}
	void OnCollisionEnter2D(Collision2D coll)
	{
		if(coll.gameObject.name.Contains("Player"))
		{
			GameManager.instance.GamePlay.SetActive(false); // disables gameplay object set
			GameManager.instance.LoseScreen.SetActive(true); // enables losescreen object set
		}
	}
}
