﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LargeHearing : MonoBehaviour 
{
	public bool playerInCollider = false; // bool to store if player is in collider and pre-sets to false
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (playerInCollider)
		{
			if(GameManager.instance.PC.noiseLevel >= 5)
			{
				GameManager.instance.AI.canHearPlayer = true; // sets bool to true
			}
			else
			{
				GameManager.instance.AI.canHearPlayer = false; // sets bool to false
			}
		}
		else
		{
			GameManager.instance.AI.canHearPlayer = false; // sets bool to false
		}
	}
	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.name.Contains("Player"))
		{	
			playerInCollider = true; // sets bool to true
		}
	}
	void OnTriggerExit2D(Collider2D coll)
	{
		if (coll.gameObject.name.Contains("Player"))
		{
			playerInCollider = false; // sets bool to false
		}
	}
}
