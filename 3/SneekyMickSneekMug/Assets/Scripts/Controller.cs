﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller : MonoBehaviour 
{
	public Vector3 home; // var to store object home location
	public Pawn pawn; // var to store pawn script
	// Use this for initialization
	public virtual void Start () 
	{
		pawn = GetComponent<Pawn>(); // gets pawn component and stores it
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
