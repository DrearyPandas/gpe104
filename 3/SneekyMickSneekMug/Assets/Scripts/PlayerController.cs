﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller 
{
	public float noiseLevel = 0f; // stores float for noise level

	// Use this for initialization
	public override void Start () 
	{
		GameManager.instance.PC = this;
		base.Start();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKey(KeyCode.W))
		{
			pawn.Forward(); // calls the forward function in pawn
		}
		if (Input.GetKey(KeyCode.A))
		{
			pawn.Left(); // calls the left function in pawn
		}
		if (Input.GetKey(KeyCode.S))
		{
			pawn.Back(); // calls the back function in pawn
		}
		if (Input.GetKey(KeyCode.D))
		{
			pawn.Right(); // calls the right function in pawn
		}
		if (Input.GetKeyDown(KeyCode.LeftShift))
		{
			pawn.Sprint(); // calls the sprint function in pawn
		}
		if (Input.GetKeyUp(KeyCode.LeftShift))
		{
			pawn.StoppedSprint(); // calls the stoppedsprint function in pawn
		}
	}
}
