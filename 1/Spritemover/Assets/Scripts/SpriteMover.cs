﻿using UnityEngine;
using System.Collections;

public class SpriteMover : MonoBehaviour 
{

    public float speed;	//Variable to allow user to alter speed
	private bool shift = false;
    private Transform location;       //Stores reference to rigidbody component

    // Use this for initialization
    void Start()
    {
        //Get and store a reference to the Rigidbody2D component
        location = GetComponent<Transform>();
    }

    //FixedUpdate is independent of framerate. Better for physics.
    void Update()
    {
		if(Input.GetKey(KeyCode.LeftShift))
		{
			shift = true;
		}
		
		if(shift == false)
		{
			//Stores horizontal input as float
			float moveH = Input.GetAxis ("Horizontal");

			//Stores vertical input as float
			float moveV = Input.GetAxis ("Vertical");

			//Uses the two vectors to generate a new one
			Vector3 movement = new Vector3 (moveH, moveV);

			//Call the AddForce function to add force to rigidbody
			location.position = location.position + (movement * speed);
		}
		
		if(shift == true)
		{
			if(Input.GetKeyDown(KeyCode.W))
			{
				location.position = location.position + new Vector3 (0, 1, 0);
			}
			if(Input.GetKeyDown(KeyCode.D))
			{
				location.position = location.position + new Vector3 (1, 0, 0);
			}
			if(Input.GetKeyDown(KeyCode.A))
			{
				location.position = location.position + new Vector3 (-1, 0, 0);
			}
			if(Input.GetKeyDown(KeyCode.S))
			{
				location.position = location.position + new Vector3 (0, -1, 0);
			}
			shift = false;
		}
    }
}