﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteReset : MonoBehaviour 
{
	private Transform sprite;

	// Use this for initialization
	void Start () 
	{
		sprite = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKey(KeyCode.Space))
		{
			
			sprite.position = new Vector3 (0, 0, -2);
		}
	}
}
