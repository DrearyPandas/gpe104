﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteDisabler : MonoBehaviour 
{
	
	private SpriteMover mover;


	// Use this for initialization
	void Start () 
	{
		mover = GetComponent<SpriteMover>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyUp(KeyCode.P))
		{
			mover.enabled = !mover.enabled;
		}
	}
}
